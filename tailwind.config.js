module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
    './src/**/*.jsx',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    theme: {
      container: {
        center: true,
      },
    },
    extend: {
      colors: {
        primary: '#13b188',
        secondary: '#ffda30',
        button: '#282c35',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('tailwindcss-textshadow')
  ],
}
