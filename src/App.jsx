import React, { useState } from "react";
import QrReader from "react-qr-scanner";
import { Base64 } from "js-base64";

const noop = () => {};

const AimCursorBorder = () => {
  const thick = 10;
  const barWidth = 30;

  return (
    <>
      <div
        style={{ top: 0, left: -1 * thick, width: thick, height: barWidth }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{
          top: -1 * thick,
          left: -1 * thick,
          width: thick + barWidth,
          height: thick,
        }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{ top: 0, right: -1 * thick, width: thick, height: barWidth }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{
          top: -1 * thick,
          right: -1 * thick,
          width: thick + barWidth,
          height: thick,
        }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{ bottom: 0, left: -1 * thick, width: thick, height: barWidth }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{
          bottom: -1 * thick,
          left: -1 * thick,
          width: thick + barWidth,
          height: thick,
        }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{ bottom: 0, right: -1 * thick, width: thick, height: barWidth }}
        className="bg-primary absolute"
      ></div>
      <div
        style={{
          bottom: -1 * thick,
          right: -1 * thick,
          width: thick + barWidth,
          height: thick,
        }}
        className="bg-primary absolute"
      ></div>
    </>
  );
};

const getTime = () => {
  const timezoneOffset = new Date().getTimezoneOffset() * 60000;
  const datetime = new Date(Date.now() - timezoneOffset)
    .toISOString()
    .replace("T", " ")
    .split(":");
  datetime.pop();
  return datetime.join(":");
};

const CloseSvg = ({ className }) => (
  <svg
    className={className}
    fill="currentColor"
    viewBox="0 0 20 20"
    role="img"
    aria-hidden="true"
  >
    <path
      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
      clipRule="evenodd"
      fillRule="evenodd"
    ></path>
  </svg>
);

const CheckMarkCircleSvg = ({ className }) => (
  <svg
    className={className}
    fill="currentColor"
    viewBox="0 0 24 24"
    fillRule="evenodd"
    clipRule="evenodd"
  >
    <path d="M12 0c6.623 0 12 5.377 12 12s-5.377 12-12 12-12-5.377-12-12 5.377-12 12-12zm0 1c6.071 0 11 4.929 11 11s-4.929 11-11 11-11-4.929-11-11 4.929-11 11-11zm7 7.457l-9.005 9.565-4.995-5.865.761-.649 4.271 5.016 8.24-8.752.728.685z" />
  </svg>
);

function App() {
  const [showAimCursor, setShowAimCursor] = useState(false);
  const [placeName, setPlaceName] = useState(null);

  return (
    <div className="flex flex-col container mx-auto h-screen">
      {!placeName ? (
        <>
          <header className="flex justify-center items-center h-12 bg-primary p-5">
            <h1 className="text-white text-shadow-md">掃瞄二維碼</h1>
          </header>
          <div className="relative flex-grow overflow-hidden">
            {showAimCursor && (
              <div
                className="h-60 w-60 absolute top-1/2 left-1/2 z-10 transform -translate-x-1/2 -translate-y-1/2 box-content border-black border-opacity-50"
                style={{ borderWidth: 1000 }}
              >
                <AimCursorBorder />
                <div className="absolute -bottom-16 left-1/2 transform -translate-x-1/2  text-white text-shadow-md">
                  掃瞄二維碼
                </div>
              </div>
            )}
            <QrReader
              onLoad={() => setShowAimCursor(true)}
              onScan={(text) => {
                if (text !== null) {
                  try {
                    const { nameZh } = JSON.parse(
                      Base64.decode(text.substr(6)).replace(/^(.*?){/, "{")
                    );
                    setPlaceName(nameZh);
                  } catch (e) {}
                }
              }}
              onError={noop}
              className="w-full h-full top-0 left-0 object-cover"
              style={{
                objectFit: "cover",
                width: '100%',
                height: '100%',
              }}
              facingMode="rear"
            />
          </div>
        </>
      ) : (
        <div className="bg-primary h-screen flex flex-col items-center text-white text-shadow-md p-8 justify-between">
          <CloseSvg
            className="w-8 h-8 self-end"
            onClick={() => setPlaceName(null)}
          />
          <div className="text-center space-y-1">
            <div>你已進入場所</div>
            <div className="text-secondary text-4xl">{placeName}</div>
            <div>{getTime()}</div>
          </div>
          <CheckMarkCircleSvg className="text-white w-36 h-36" />
          <div className="text-center space-y-1">
            <button
              className="bg-secondary rounded-3xl m-2 px-28 py-1.5 text-button text-2xl shadow-lg"
              onClick={() => setPlaceName(null)}
            >
              離開
            </button>
            <div>當你離開時請緊記按"離開"</div>
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
